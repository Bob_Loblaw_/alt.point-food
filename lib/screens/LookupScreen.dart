import 'package:alt_point_food/models/food.dart';
import 'package:alt_point_food/screens/widgets/CustomDrawer.dart';
import 'package:alt_point_food/screens/widgets/LookupMainContents.dart';
import 'package:alt_point_food/screens/widgets/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LookupScreen extends StatelessWidget {

  final Food _food;

  LookupScreen(this._food) : super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
      drawer: CustomDrawer(),
      body: Stack(
        children: [
          LookupMainContents(_food),
          _buildNavOverlay(context)
        ]
      )
    );
  }

  Widget _buildNavOverlay(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 45, left: 45, top: 60),
      child: Container(
          height: 45,
          child: Row(
            children: [
              Container(
                child: ClipOval(
                  child: GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      height: 45,
                      width: 45,
                      color: Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
                      child: SvgPicture.asset('assets/backArrow.svg'),
                      alignment: Alignment.center,
                    ),
                  ),
                ),
                alignment: Alignment.centerLeft,
              ),
              Expanded(
                child: Container(
                  child: ClipOval(
                    child: Container(
                      height: 45,
                      width: 45,
                      color: Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
                      child: CartButton(),
                      alignment: Alignment.center,
                    ),
                  ),
                  alignment: Alignment.centerRight,
                ),
              )
            ],
          )
      ),
    );
  }
}

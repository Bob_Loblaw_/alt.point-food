import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Collapsable extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      automaticallyImplyLeading: false,
      collapsedHeight: 150,
      floating: true,
      flexibleSpace: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.only(bottom: 30, top: 30, left: 40), child: Text('Голоден?', style: TextStyle(fontSize: 36, fontStyle: FontStyle.normal))),
            Padding(
              padding: EdgeInsets.only(left: 40),
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SvgPicture.asset('assets/search.svg'),
                    Padding(padding: EdgeInsets.only(left: 19), child: Text('Поиск по меню', style: TextStyle(color: Color.fromARGB(0xff, 0xbd, 0xbd, 0xbd), fontSize: 16)))
                  ],
                ),
              ),
            )
          ]
        )
      ),
      elevation: 0
    );
  }
}

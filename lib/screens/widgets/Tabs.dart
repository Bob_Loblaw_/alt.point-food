import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Tabs extends StatefulWidget {
  @override
  _TabsState createState() => _TabsState();
}

class _TabsState extends State<Tabs> {

  int chosen = 0;

  @override
  Widget build(BuildContext context) {
    return SliverPersistentHeader(
      pinned: true,
      delegate: TabsDelegate(
        Container(
          color: Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
          child: Row(
            children: [
              GestureDetector(
                onTap: () => setState(() {chosen = 0;}),
                child: Container(width: 60, height: 60, child: Column(
                  children: [
                    SvgPicture.asset('assets/all.svg'),
                    SizedBox(height: 10),
                    Text('Все', style: TextStyle(fontSize: 12, fontWeight: chosen == 0 ? FontWeight.bold : FontWeight.normal))
                  ],
                  mainAxisAlignment: MainAxisAlignment.end,
                )),
              ),
              GestureDetector(
                onTap: () => setState(() {chosen = 1;}),
                child: Container(width: 60, height: 60, child: Column(
                  children: [
                    SvgPicture.asset('assets/pizza.svg'),
                    SizedBox(height: 10),
                    Text('Пицца', style: TextStyle(fontSize: 12, fontWeight: chosen == 1 ? FontWeight.bold : FontWeight.normal))
                  ],
                  mainAxisAlignment: MainAxisAlignment.end,
                )),
              ),
              GestureDetector(
                onTap: () => setState(() {chosen = 2;}),
                child: Container(width: 60, height: 60, child: Column(
                  children: [
                    SvgPicture.asset('assets/kebab.svg'),
                    SizedBox(height: 10),
                    Text('Кебаб', style: TextStyle(fontSize: 12, fontWeight: chosen == 2 ? FontWeight.bold : FontWeight.normal))
                  ],
                  mainAxisAlignment: MainAxisAlignment.end,
                )),
              ),
              GestureDetector(
                onTap: () => setState(() {chosen = 3;}),
                child: Container(width: 60, height: 60, child: Column(
                  children: [
                    SvgPicture.asset('assets/borga.svg'),
                    SizedBox(height: 10),
                    Text('Бургер', style: TextStyle(fontSize: 12, fontWeight: chosen == 3 ? FontWeight.bold : FontWeight.normal))
                  ],
                  mainAxisAlignment: MainAxisAlignment.end,
                )),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly
          )
        )
      )
    );
  }
}

class TabsDelegate extends SliverPersistentHeaderDelegate {

  TabsDelegate(this.child);

  final Widget child;

  @override
  double get minExtent => 100;

  @override
  double get maxExtent => 100;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(TabsDelegate oldDelegate) {
    return true;
  }
}
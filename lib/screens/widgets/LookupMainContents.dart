import 'package:alt_point_food/models/food.dart';
import 'package:alt_point_food/screens/widgets/MenuItem.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LookupMainContents extends StatelessWidget {

  final Food _food;

  LookupMainContents(this._food) : super();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          _buildPicFrame(),
          _buildDescription(),
          _buildAddon(),
          _buildAddon(),
          _buildAddon(),
          _buildAddon(),
        ],
      ),
    );
  }

  Widget _buildPicFrame() {
    return SizedBox(
      height: 260,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(_food.pic),
                fit: BoxFit.cover
            )
        ),
      ),
    );
  }

  Widget _buildDescription() {
    return Container(
      padding: EdgeInsets.only(top: 24, left: 45, right: 45),
      child: Column(
        children: [
          Container(
            height: 24,
            child: SvgPicture.asset('assets/heart.svg'),
            alignment: Alignment.centerRight,
          ),
          Padding(padding: EdgeInsets.only(bottom: 30), child: InfoBox(_food, true)),
          LookupBottomButtons()
        ]
      )
    );
  }

  Widget _buildAddon() {
    return Padding(
      padding: EdgeInsets.only(right: 30, left: 30, top: 7, bottom: 7),
      child: ListTile(
        leading: SizedBox(height: 60, width: 60, child: Image.asset('assets/coffeeCup.png')),
        title: Row(
          children: [
            Expanded(
              flex: 2,
              child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Просто кофе',
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              )
            ),
            Expanded(
              child: Container(
                alignment: Alignment.centerRight,
                child: Text(
                  '200 руб',
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              )
            ),
          ],
        ),
        subtitle: Text('Вкусный зерновой кофеёчек'),
      ),
    );
  }
}

class LookupBottomButtons extends StatefulWidget {
  @override
  _LookupBottomButtonsState createState() => _LookupBottomButtonsState();
}

class _LookupBottomButtonsState extends State<LookupBottomButtons> {

  int chosen = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: GestureDetector(
              onTap: () {setState(() => chosen = 0);},
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 15),
                    child: Text(
                      'Дополнительно',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: chosen == 0 ? Color.fromARGB(0xff, 0x33, 0x33, 0x33) : Color.fromARGB(0xff, 0x82, 0x82, 0x82),
                      )
                    ),
                    alignment: Alignment.centerLeft,
                  ),
                  Container(
                    width: 45,
                    height: 2,
                    color: chosen == 0 ? Color.fromARGB(0xff, 0x33, 0x33, 0x33) : Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: GestureDetector(
              onTap: () {setState(() => chosen = 1);},
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 15),
                    child: Text(
                      'Похожие',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: chosen == 1 ? Color.fromARGB(0xff, 0x33, 0x33, 0x33) : Color.fromARGB(0xff, 0x82, 0x82, 0x82),
                      )
                    ),
                    alignment: Alignment.center,
                  ),
                  Container(
                    width: 45,
                    height: 2,
                    color: chosen == 1 ? Color.fromARGB(0xff, 0x33, 0x33, 0x33) : Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: GestureDetector(
              onTap: () {setState(() => chosen = 2);},
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 15),
                    child: Text(
                      'Отзывы',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: chosen == 2 ? Color.fromARGB(0xff, 0x33, 0x33, 0x33) : Color.fromARGB(0xff, 0x82, 0x82, 0x82),
                      )
                    ),
                    alignment: Alignment.centerRight,
                  ),
                  Container(
                    width: 45,
                    height: 2,
                    color: chosen == 2 ? Color.fromARGB(0xff, 0x33, 0x33, 0x33) : Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
